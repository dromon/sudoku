#! /usr/bin/env ruby
# frozen_string_literal: true

require 'ncursesw'
require 'set'
require_relative 'interface'

STDOUT.sync = false

require 'sudoku/version'

module Sudoku
  class Error < StandardError; end
  # Должен быть решатель.
  # Решение судоку с помощью алгоритма, поиск единственно возможных значений
  class AlgoSolution
    attr_accessor :sudoku_array
    def initialize(sudoku_array)
      @sudoku_array = sudoku_array
    end

    # Поиск единственного решения
    def algorythm
      (0...9).each do |row|
        (0...9).each do |col|
          filling_possible
          if @possible_values[[row, col]]
            if value_in_row(row, col).size == 1
              sudoku_array[row][col] = value_in_row(row, col).to_a[0]
            elsif value_in_col(row, col).size == 1
              sudoku_array[row][col] = value_in_col(row, col).to_a[0]
            elsif value_in_block(row, col).size == 1
              sudoku_array[row][col] = value_in_block(row, col).to_a[0]
            end
          end
        end
      end
    end

    # Заполнение хэша возможными значениями
    def filling_possible
      @possible_values = {}
      (0...9).each do |row|
        (0...9).each do |col|
          pre_possible_values = Set[]
          next unless @sudoku_array[row][col].zero?

          (1..9).each do |value|
            pre_possible_values.add(value) if check(row, col, value) == false
          end
          @possible_values[[row, col]] = pre_possible_values
        end
      end
    end

    # Берет единственное значение в строкеpre_possible_values.clear
    def value_in_row(row, num_col)
      tmp = @possible_values[[row, num_col]]
      (0...9).each do |col|
        tmp -= @possible_values[[row, col]].to_a if (col != num_col) && @possible_values[[row, col]]
      end
      tmp
    end

    # Берет единственное значение в столбце
    def value_in_col(num_row, col)
      tmp = @possible_values[[num_row, col]]
      (0...9).each do |row|
        tmp -= @possible_values[[row, col]].to_a if (row != num_row) && @possible_values[[row, col]]
      end
      tmp
    end

    # Берет единственное значение в блоке
    def value_in_block(row, col)
      tmp = @possible_values[[row, col]]
      square_start_row = 3 * (row / 3)
      square_start_col = 3 * (col / 3)
      tmp_i = square_start_row - 1
      tmp_j = square_start_col - 1
      while (tmp_j += 1) <= square_start_col + 2
        while (tmp_i += 1) <= square_start_row + 2
          if ((tmp_i != row) || (tmp_j != col)) && @possible_values[[tmp_i, tmp_j]]
            tmp -= @possible_values[[tmp_i, tmp_j]].to_a
          end
        end
        tmp_i = square_start_row - 1
      end
      tmp
    end

    # Проверка на принадлежность числа всему, если принадлежит - true
    def check(row, col, value)
      return true if check_row(row, value) == true
      return true if check_col(col, value) == true
      return true if check_square(row, col, value) == true

      false
    end

    # Проверка на принадлежность числа строке, если принадлежит - true
    def check_row(row, value)
      sudoku_array[row].include? value
    end

    # Проверка на принадлежность чиcла столбцу, если принадлежит - true
    def check_col(col, value)
      sudoku_array.transpose[col].include? value
    end

    # Проверка на принадлежность чичла квадрату, если принадлежит - true
    def check_square(row, col, value)
      square_start_row = 3 * (row / 3)
      square_start_col = 3 * (col / 3)
      i = square_start_row - 1
      j = square_start_col - 1
      while (j += 1) <= square_start_col + 2
        while (i += 1) <= square_start_row + 2
          return true if sudoku_array[i][j] == value
        end
        i = square_start_row - 1
      end
      false
    end
  end

  # Решение перебором, преобразовываем массив в одномерный и перебираем оставшиеся
  # значения, не используя уже возможные значения из первого алгоритма
  class RandSolution
    attr_accessor :sudoku_array

    def initialize(sudoku_array)
      @sudoku_array = sudoku_array
    end

    def all_check(sudoku_array)
      i = sudoku_array.index(0)
      if i.nil?
        @sudoku_array = sudoku_array.map(&:dup)
        return
      end
      impossible_values = []
      (0...81).each do |j|
        next unless (i / 9 == j / 9) ||
                    ((i - j) % 9).zero? ||
                    ((i / 27 == j / 27) && (i % 9 / 3 == j % 9 / 3))

        impossible_values |= [sudoku_array[j]]
      end
      (1..9).each do |n|
        all_check(sudoku_array[0...i] + [n] + sudoku_array[i + 1...81]) if impossible_values.include?(n) == false
      end
    end

    # Преобразовываем массив обратно в двухмерный, чтобы вывод работал
    def d2
      sudoku_array = Array.new(9).map! { Array.new(9, 0) }
      ind = -1
      (0...9).each do |row|
        (0...9).each do |col|
          ind += 1
          sudoku_array[row][col] = @sudoku_array[ind]
        end
      end
      sudoku_array
    end
  end
end
