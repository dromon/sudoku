# frozen_string_literal: true

# Interface with ncursesw
class UserInterfaceNcurses
  def initialize(scr)
    @scr = scr
  end

  def print_sudoku_border(col)
    @scr.attron(Ncurses.COLOR_PAIR(2))
    row = 3
    lib = File.expand_path('border.txt', __dir__)
    File.read(lib).each_line do |line|
      row += 1
      @scr.mvaddstr(row, col, line)
    end
    @scr.attroff(Ncurses.COLOR_PAIR(2))
  end

  def filling_sudoku_border(sudoku_array, col, coloured = false)
    @scr.attron(Ncurses.COLOR_PAIR(2))
    row = 4
    (0...9).each do |i|
      row += 1
      col_tmp = col
      (0...9).each do |j|
        col_tmp += 2 if (j == 3) || (j == 6)
        col_tmp += 2
        @scr.mvaddstr(row, col_tmp, sudoku_array[i][j].to_s) if sudoku_array[i][j] != 0
        next unless coloured && @sudoku_array_old[i][j].zero?

        @scr.attron(Ncurses.COLOR_PAIR(1))
        @scr.mvaddstr(row, col_tmp, sudoku_array[i][j].to_s)
        @scr.attroff(Ncurses.COLOR_PAIR(1))
      end
      row += 1 if (i == 2) || (i == 5)
    end
    @scr.refresh
    @scr.attroff(Ncurses.COLOR_PAIR(2))
  end

  def print_kitten
    row = 3
    lib = File.expand_path('kitten.txt', __dir__)
    File.read(lib).each_line do |line|
      row += 1
      @scr.move(row, 25)
      @scr.attron(Ncurses.COLOR_PAIR(3))
      @scr.waddstr(line)
      @scr.attroff(Ncurses.COLOR_PAIR(3))
    end
  end

  def starter_window
    loop do
      @scr.clear
      @scr.mvaddstr(1, 30, 'Решатель СУДОКУ 3001')
      print_kitten
      @scr.mvaddstr(18, 1, 'Выберите действие:')
      @scr.mvaddstr(19, 1, '1 - для выбора судоку из файла, 2 - для выбора генерации судоку, q - для выхода')
      ch = @scr.getch
      case ch
      when '1'.ord
        read_file
        what_to_do_with_sudoku
      when '2'.ord
        @scr.mvaddstr(22, 1, 'Подождите реализации этой функции Машей ;)')
        @scr.refresh
        sleep 1
      when 'q'.ord
        @scr.clear
        @scr.refresh
        exit
      end
    end
  end

  def what_to_do_with_sudoku
    loop do
      @scr.clear
      @scr.mvaddstr(1, 30, 'Решатель СУДОКУ 3001')
      print_sudoku_border(10)
      filling_sudoku_border(@sudoku_array, 10, @scr)
      @scr.mvaddstr(18, 1, 'Выберите действие:')
      @scr.mvaddstr(19, 1, '1 - авторешение, 2 - самостоятельное решение, q - для выхода')
      ch = @scr.getch
      case ch
      when '1'.ord
        auto_solver
        starter_window
      when '2'.ord
        solution
        user_solving
      when 'q'.ord
        @scr.clear
        @scr.refresh
        exit
      end
    end
  end
  # Отдельный класс солвер

  def solution
    searching_pairs = Sudoku::AlgoSolution.new(@sudoku_array)
    checking_all = Sudoku::RandSolution.new(@sudoku_array)
    while @sudoku_array.any? { |row| row.include?(0) }
      sudoku_array_new = @sudoku_array.map(&:dup)
      searching_pairs.algorythm
      next unless sudoku_array_new == @sudoku_array

      @sudoku_array.flatten!
      checking_all.all_check(@sudoku_array)
      @sudoku_array = checking_all.d2.map(&:dup)
    end
  end

  def auto_solver
    solution
    @scr.clear
    @scr.mvaddstr(1, 30, 'Решатель СУДОКУ 3001')
    print_sudoku_border(10)
    filling_sudoku_border(@sudoku_array_old, 10)
    print_sudoku_border(40)
    filling_sudoku_border(@sudoku_array, 40, true)
    @scr.mvaddstr(23, 5, 'Всё решено, я великолепна :) нажмите кнопку, чтобы начать сначала')
    @scr.refresh
    @scr.getch
  end

  def navigation_files(len)
    cursor_pos = 0
    loop do
      @scr.move(20 + cursor_pos, 0)
      @scr.mvaddstr(20 + cursor_pos, 0, '→ ')
      ch = @scr.getch
      case ch
      when Ncurses::KEY_UP
        @scr.mvaddstr(20 + cursor_pos, 0, '  ')
        cursor_pos = [cursor_pos - 1, 0].max
      when Ncurses::KEY_DOWN
        if cursor_pos < len - 1
          @scr.mvaddstr(20 + cursor_pos, 0, '  ')
          cursor_pos = [cursor_pos + 1, 0].max
        end
      when Ncurses::KEY_ENTER, "\n".ord
        return cursor_pos
      else
        Ncurses.beep
      end
    end
  end

  def read_file
    @scr.clear
    files = []
    @scr.mvaddstr(1, 30, 'Решатель СУДОКУ 3001')
    print_kitten
    @scr.mvaddstr(18, 1, ' Выберите файл:')
    # Навигатор по файлам в папке .git, потом нужно перенести в другое место
    str = 19
    Dir.chdir('.git') do
      Dir['*.txt'].each do |file_name|
        str += 1
        files << file_name
        @scr.mvaddstr(str, 2, file_name)
      end
    end
    row = navigation_files(files.size)
    @scr.refresh
    name_of_file = files[row]
    @scr.mvaddstr(str + 2, 0, 'Открываю файл с названием ' + name_of_file)
    @scr.refresh
    @sudoku_array = File.read('.git/' + name_of_file).strip.each_line.map { |line| line.strip.each_char.map(&:to_i) }
    @sudoku_array_old = @sudoku_array.map(&:dup)
    @sudoku_array_user = @sudoku_array.map(&:dup)
    sleep(1)
  end

  def user_solving
    @scr.clear
    @scr.mvaddstr(1, 30, 'Решатель СУДОКУ 3001')
    @scr.mvprintw(18, 1, 's - проверить решение, i - подсказка, q - выход ')
    print_sudoku_border(10)
    filling_sudoku_border(@sudoku_array_old, 10)
    Ncurses.curs_set(1)
    row = 5
    col = 12
    ind_arr_col = 0
    ind_arr_row = 0
    cursor_pos_col = 0
    cursor_pos_row = 0
    @scr.attron(Ncurses.COLOR_PAIR(4))
    loop do
      @scr.move(row + cursor_pos_row, col + cursor_pos_col)
      ch = @scr.getch
      case ch
      when Ncurses::KEY_LEFT
        if ind_arr_col - 1 >= 0
          ind_arr_col -= 1
          cursor_pos_col = if ind_arr_col % 3 == 2
                             cursor_pos_col - 4
                           else
                             cursor_pos_col - 2
                           end
        else
          Ncurses.beep
        end
      when Ncurses::KEY_RIGHT
        if ind_arr_col < 8
          ind_arr_col += 1
          cursor_pos_col = if (ind_arr_col % 3).zero?
                             cursor_pos_col + 4
                           else
                             cursor_pos_col + 2
                           end
        else
          Ncurses.beep
        end
      when Ncurses::KEY_UP
        if ind_arr_row - 1 >= 0
          ind_arr_row -= 1
          cursor_pos_row = if ind_arr_row % 3 == 2
                             cursor_pos_row - 2
                           else
                             cursor_pos_row - 1
                           end
        else
          Ncurses.beep
        end
      when Ncurses::KEY_DOWN
        if ind_arr_row + 1 < 9
          ind_arr_row += 1
          cursor_pos_row = if (ind_arr_row % 3).zero?
                             cursor_pos_row + 2
                           else
                             cursor_pos_row + 1
                           end
        else
          Ncurses.beep
        end
      when '1'.ord..'9'.ord
        if @sudoku_array_old[ind_arr_row][ind_arr_col].zero?
          @scr.mvprintw(row + cursor_pos_row, col + cursor_pos_col, ch.chr.to_s)
          @sudoku_array_user[ind_arr_row][ind_arr_col] = ch.chr.to_i
        end
        # when Ncurses::KEY_ENTER, "\n".ord
        # print_sudoku_border(@scr, 40)
        # filling_sudoku_border(@sudoku_array_user, 40, @scr)
        # @scr.refresh

      when Ncurses::KEY_BACKSPACE
        if @sudoku_array_old[ind_arr_row][ind_arr_col].zero?
          @scr.mvprintw(row + cursor_pos_row, col + cursor_pos_col, ' ')
          @sudoku_array_user[ind_arr_row][ind_arr_col] = 0
        end
        Ncurses.beep
      when 'q'.ord
        Ncurses.curs_set(0)
        starter_window
      when 's'.ord
        print_sudoku_border(40)
        user_solution_check(40)
      when 'i'.ord
        # Здесь должны выводиться подсказки
        @scr.mvprintw(20, 1, 'МАША СДЕЛАЙ ПОДСКАЗКИ Я НЕ МОГУ РЕШИТЬ')
      else
        Ncurses.beep
      end
      @scr.refresh
    end
    @scr.attroff(Ncurses.COLOR_PAIR(4))
  end

  def user_solution_check(col)
    @scr.attron(Ncurses.COLOR_PAIR(2))
    row = 4
    (0...9).each do |i|
      row += 1
      col_tmp = col
      (0...9).each do |j|
        # Если индексы 3, 6, нужно доп символ отступить
        col_tmp += 2 if (j == 3) || (j == 6)
        col_tmp += 2
        @scr.mvaddstr(row, col_tmp, @sudoku_array[i][j].to_s) if @sudoku_array[i][j] != 0
        next unless @sudoku_array_old[i][j].zero?

        if @sudoku_array_user[i][j] == @sudoku_array[i][j]
          @scr.attron(Ncurses.COLOR_PAIR(1))
          @scr.mvaddstr(row, col_tmp, @sudoku_array[i][j].to_s)
          @scr.attroff(Ncurses.COLOR_PAIR(1))
        else
          @scr.attron(Ncurses.COLOR_PAIR(5))
          @scr.mvaddstr(row, col_tmp, @sudoku_array_user[i][j].to_s)
          @scr.attroff(Ncurses.COLOR_PAIR(5))
        end
      end
      row += 1 if (i == 2) || (i == 5)
    end
    @scr.refresh
    @scr.attroff(Ncurses.COLOR_PAIR(2))
  end
end
