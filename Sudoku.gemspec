# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'sudoku/version'

Gem::Specification.new do |spec|
  spec.name          = 'sudoku'
  spec.version       = Sudoku::VERSION
  spec.authors       = ['Polina Grinevich']
  spec.email         = ['meadina18@gmail.com']

  spec.summary       = 'Sudoku solver'
  spec.homepage      = 'https://gitlab.com/dromon/sudoku'

  spec.files         = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '~> 2.0'
  spec.add_runtime_dependency 'ncursesw', '~> 1.4'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rubocop'
end
